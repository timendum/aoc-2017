from statistics import mode
import re

from utils import load_string, run_testCase, unittest

TEST = """pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)"""

R_LINE = re.compile(r'(?P<name>\w+)\s\((?P<weight>\d+)\)(?:\s\->\s(?P<above>\w.+))?')


def bottom_name(s):
    lines = s.splitlines()
    parents = set()
    childs = set()
    for line in lines:
        groups = R_LINE.match(line).groupdict()
        if not groups.get('above', None):
            continue
        parents.add(groups['name'])
        childs.update(groups['above'].split(', '))
    return (parents - childs).pop()


def adjust_weights(final_weights, partial_weights):
    target = mode(final_weights)
    for fw, pw in zip(final_weights, partial_weights):
        if fw > target:
            return pw - (max(final_weights) - min(final_weights))
        elif fw < target:
            return pw + (max(final_weights) - min(final_weights))


def find_unbalanced(s):
    lines = s.splitlines()
    final_weights = {}
    partial_weights = {}
    childs = {}
    for line in lines:
        groups = R_LINE.match(line).groupdict()
        if groups.get('above', None):
            childs[groups['name']] = set(groups['above'].split(', '))
            partial_weights[groups['name']] = int(groups['weight'])
        else:
            final_weights[groups['name']] = int(groups['weight'])
    while partial_weights:
        nexts = [k for k, v in childs.items() if v.issubset(final_weights.keys())]
        for elem in nexts:
            weights = [final_weights[e] for e in childs[elem]]
            if max(weights) != min(weights):
                return adjust_weights(weights, [partial_weights[e] for e in childs[elem]])
            final_weights[elem] = partial_weights[elem] + sum(weights)
            del childs[elem]


class TestDay(unittest.TestCase):
    def test_redistribution_cycles(self):
        self.assertEqual(bottom_name(TEST), "tknk")

    def test_find_unbalanced(self):
        self.assertEqual(find_unbalanced(TEST), 60)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day07.txt")
    print("Part 1: ", bottom_name(txt))
    print("Part 2: ", find_unbalanced(txt))
