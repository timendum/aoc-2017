import re

from utils import load_string, run_testCase, unittest


def redistribution_cycles(s):
    banks = [int(number) for number in re.split(r'\s', s)]
    seen = set()
    while tuple(banks) not in seen:
        seen.add(tuple(banks))
        value = max(banks)
        target = banks.index(value)
        banks[target] = 0
        while value > 0:
            target = (target + 1) % len(banks)
            banks[target] += 1
            value -= 1
    return len(seen)


def redistribution_loop(s):
    banks = [int(number) for number in re.split(r'\s', s)]
    seen = set()
    counter = 1
    looper = None
    while True:
        if looper is None:
            if tuple(banks) in seen:
                looper = tuple(banks)
            else:
                seen.add(tuple(banks))
        else:
            if looper == tuple(banks):
                break
            counter += 1
        value = max(banks)
        target = banks.index(value)
        banks[target] = 0
        while value > 0:
            target = (target + 1) % len(banks)
            banks[target] += 1
            value -= 1
    return counter


class TestDay(unittest.TestCase):
    def test_redistribution_cycles(self):
        self.assertEqual(redistribution_cycles("0 2 7 0"), 5)

    def test_redistribution_loop(self):
        self.assertEqual(redistribution_loop("0 2 7 0"), 4)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day06.txt")
    print("Part 1: ", redistribution_cycles(txt))
    print("Part 2: ", redistribution_loop(txt))
