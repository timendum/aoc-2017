from collections import deque

from utils import load_string, run_testCase, unittest


def score_groups(s):
    stack = []
    score = 0
    level = 0
    stream = deque(s)
    while stream:
        c = stream.popleft()
        if c == '{' and (not stack or stack[-1] != '<'):
            stack.append(c)
            level += 1
        elif c == '}' and stack[-1] == '{':
            stack.pop()
            score += level
            level -= 1
        elif c == '!':
            stream.popleft()
        elif c == '<' and (not stack or stack[-1] != '<'):
            stack.append(c)
        elif c == '>' and stack[-1] == '<':
            stack.pop()
    return score


def count_garbage(s):
    stack = []
    score = 0
    stream = deque(s)
    while stream:
        c = stream.popleft()
        if c == '{' and (not stack or stack[-1] != '<'):
            stack.append(c)
        elif c == '}' and stack[-1] == '{':
            stack.pop()
        elif c == '!':
            stream.popleft()
        elif c == '<' and (not stack or stack[-1] != '<'):
            stack.append(c)
        elif c == '>' and stack[-1] == '<':
            stack.pop()
        elif stack and stack[-1] == '<':
            score += 1
    return score


class TestDay(unittest.TestCase):
    def test_score_groups(self):
        self.assertEqual(score_groups("{}"), 1)
        self.assertEqual(score_groups("{{{}}}"), 6)
        self.assertEqual(score_groups("{{},{}}"), 5)
        self.assertEqual(score_groups("{{{},{},{{}}}}"), 16)
        self.assertEqual(score_groups("{<a>,<a>,<a>,<a>}"), 1)
        self.assertEqual(score_groups("{{<ab>},{<ab>},{<ab>},{<ab>}}"), 9)
        self.assertEqual(score_groups("{{<!!>},{<!!>},{<!!>},{<!!>}}"), 9)
        self.assertEqual(score_groups("{{<a!>},{<a!>},{<a!>},{<ab>}}"), 3)

    def test_count_garbage(self):
        self.assertEqual(count_garbage("{}"), 0)
        self.assertEqual(count_garbage("<>"), 0)
        self.assertEqual(count_garbage("<random characters>"), 17)
        self.assertEqual(count_garbage("<<<<>"), 3)
        self.assertEqual(count_garbage("<{!>}>"), 2)
        self.assertEqual(count_garbage("<!!>"), 0)
        self.assertEqual(count_garbage("<!!!>>"), 0)
        self.assertEqual(count_garbage("<{o\"i!a,<{i<a>"), 10)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day09.txt")
    print("Part 1: ", score_groups(txt))
    print("Part 2: ", count_garbage(txt))
