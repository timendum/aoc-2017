from utils import load_string, run_testCase, unittest

TEST_1 = """aa bb cc dd ee
aa bb cc dd aa
aa bb cc dd aaa"""

TEST_2 = """abcde fghij
abcde xyz ecdab
a ab abc abd abf abj
iiii oiii ooii oooi oooo
oiii ioii iioi iiio"""


def count_valid(s):
    lines = s.splitlines()
    lines = [line.split() for line in lines]
    return len([line for line in lines if len(line) == len(set(line))])


def count_not_anagram(s):
    lines = s.splitlines()
    lines = [line.split() for line in lines]
    lines = [[tuple(sorted(word)) for word in line] for line in lines]
    return len([line for line in lines if len(line) == len(set(line))])


class TestDay(unittest.TestCase):
    def test_count_valid(self):
        self.assertEqual(count_valid(TEST_1), 2)

    def test_count_not_anagram(self):
        self.assertEqual(count_not_anagram(TEST_2), 3)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day04.txt")
    print("Part 1: ", count_valid(txt))
    print("Part 2: ", count_not_anagram(txt))
