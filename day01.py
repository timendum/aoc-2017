import unittest

from utils import load_string, run_testCase


def next_sum(s):
    r = 0
    for i in range(len(s)):
        if s[i] == s[i - 1]:
            r += int(s[i])
    return (r)


def halfwaysum(s):
    r = 0
    half = int(len(s) / 2)
    for i in range(half):
        if s[i] == s[half + i]:
            r += int(s[i]) * 2
    return (r)


class TestDay01(unittest.TestCase):
    def test_next_sum(self):
        self.assertEqual(next_sum("1122"), 3)
        self.assertEqual(next_sum("1111"), 4)
        self.assertEqual(next_sum("1234"), 0)
        self.assertEqual(next_sum("91212129"), 9)

    def test_halfwaysum(self):
        self.assertEqual(halfwaysum("1212"), 6)
        self.assertEqual(halfwaysum("1221"), 0)
        self.assertEqual(halfwaysum("123425"), 4)
        self.assertEqual(halfwaysum("123123"), 12)
        self.assertEqual(halfwaysum("12131415"), 4)


if __name__ == '__main__':
    run_testCase(TestDay01)
    txt = load_string("day01.txt")
    print("Part 1: ", next_sum(txt))
    print("Part 2: ", halfwaysum(txt))
