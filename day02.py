import unittest

from utils import load_string, run_testCase


def diff_checksum(s: str):
    lines = s.splitlines()
    r = 0
    for line in lines:
        snumbers = line.split('\t')
        numbers = [int(snumber) for snumber in snumbers]
        r += max(numbers) - min(numbers)
    return r


def even_checksum(s: str):
    lines = s.splitlines()
    r = 0
    for line in lines:
        snumbers = line.split('\t')
        numbers = [int(snumber) for snumber in snumbers]
        for number in numbers:
            remainers = [int(number / n) for n in numbers if number % n == 0 and n != number]
            if sum(remainers) > 0:
                r += sum(remainers)
                break
    return r


class TestDay02(unittest.TestCase):
    def test_diff_checksum(self):
        txt = """5	1	9	5
7	5	3
2	4	6	8"""
        self.assertEqual(diff_checksum(txt), 18)

    def test_even_checksum(self):
        txt = """5	9	2	8
9	4	7	3
3	8	6	5"""
        self.assertEqual(even_checksum(txt), 9)


if __name__ == '__main__':
    run_testCase(TestDay02)
    txt = load_string("day02.txt")
    print(diff_checksum(txt))
    print(even_checksum(txt))
