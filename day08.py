from collections import defaultdict

from utils import load_string, run_testCase, unittest

TEST = """b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"""


def check_condition(condition, registers):
    operations = {
        '>': lambda a, b: a > b,
        '>=': lambda a, b: a >= b,
        '<': lambda a, b: a < b,
        '<=': lambda a, b: a <= b,
        '==': lambda a, b: a == b,
        '!=': lambda a, b: a != b,
    }
    return operations[condition[2]](registers[condition[1]], int(condition[3]))


def find_larger(s):
    lines = s.splitlines()
    registers = defaultdict(int)
    for line in lines:
        ops = line.split(' ')
        if check_condition(ops[3:], registers):
            if ops[1] == 'inc':
                registers[ops[0]] += int(ops[2])
            elif ops[1] == 'dec':
                registers[ops[0]] -= int(ops[2])
    return max(registers.values())


def find_max(s):
    lines = s.splitlines()
    registers = defaultdict(int)
    maximum = 0
    for line in lines:
        ops = line.split(' ')
        if check_condition(ops[3:], registers):
            if ops[1] == 'inc':
                registers[ops[0]] += int(ops[2])
            elif ops[1] == 'dec':
                registers[ops[0]] -= int(ops[2])
        maximum = max(maximum, *registers.values())
    return maximum


class TestDay(unittest.TestCase):
    def test_find_larger(self):
        self.assertEqual(find_larger(TEST), 1)

    def test_find_max(self):
        self.assertEqual(find_max(TEST), 10)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day08.txt")
    print("Part 1: ", find_larger(txt))
    print("Part 2: ", find_max(txt))
