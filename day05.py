from utils import load_string, run_testCase, unittest

TEST = """0
3
0
1
-3"""


def step_outside(s):
    numbers = [int(line) for line in s.splitlines()]
    counter = 0
    steps = 0
    while counter >= 0 and counter < len(numbers):
        steps += 1
        numbers[counter] += 1
        counter += numbers[counter] - 1
    return steps


def strange_outside(s):
    numbers = [int(line) for line in s.splitlines()]
    counter = 0
    steps = 0
    offset = 0
    while counter >= 0 and counter < len(numbers):
        steps += 1
        offset = numbers[counter]
        if numbers[counter] >= 3:
            numbers[counter] -= 1
        else:
            numbers[counter] += 1
        counter += offset
    return steps


class TestDay(unittest.TestCase):
    def test_step_outside(self):
        self.assertEqual(step_outside(TEST), 5)

    def test_strange_outside(self):
        self.assertEqual(strange_outside(TEST), 10)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day05.txt")
    print("Part 1: ", step_outside(txt))
    print("Part 2: ", strange_outside(txt))
