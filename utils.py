import unittest
import os

def run_testCase(testcase):
    unittest.TextTestRunner().run(unittest.TestLoader().loadTestsFromTestCase(testcase))

def load_string(filename):
    with open(os.path.join('data', filename) , 'rt') as ifile:
        return ifile.read().strip()
