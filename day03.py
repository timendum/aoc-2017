import unittest
from collections import defaultdict
from math import ceil, sqrt

from utils import load_string, run_testCase


def manhattan_distance(s):
    n = int(s)
    qsize = ceil(sqrt(n))
    qsize += 1 if qsize % 2 == 0 else 0
    x = int((qsize - 1) / 2)
    y = ((qsize * qsize) - n) % (qsize - 1)
    y = int(abs((qsize - 1) / 2 - y))
    return x + y


def coord_gen():
    x = 0
    y = 0
    while True:
        yield (x, y)
        if y > -x and x > y:  # up
            y += 1
        elif y > -x and y >= x:  # left
            x -= 1
        elif y <= -x and x < y:  # down
            y -= 1
        elif y <= -x and x >= y:  # right
            x += 1


def adjacent(point):
    x, y = point
    return [(x + dx, y + dy) for dx in range(-1, 2) for dy in range(-1, 2)]


def spiral_sum(s):
    n = int(s)
    plot = defaultdict(int)
    coords = coord_gen()
    plot[next(coords)] = 1
    for _, coord in zip(range(1, n), coords):
        plot[coord] = sum([plot[near] for near in adjacent(coord)])
        if plot[coord] > n:
            return plot[coord]


class TestDay(unittest.TestCase):
    def test_manhattan_distance(self):
        #self.assertEqual(manhattan_distance("1"), 0)
        self.assertEqual(manhattan_distance("12"), 3)
        self.assertEqual(manhattan_distance("10"), 3)
        self.assertEqual(manhattan_distance("23"), 2)
        self.assertEqual(manhattan_distance("1024"), 31)

    def test_coord_gen(self):
        coords = coord_gen()
        self.assertEqual(next(coords), (0, 0))
        self.assertEqual(next(coords), (1, 0))
        self.assertEqual(next(coords), (1, 1))
        self.assertEqual(next(coords), (0, 1))
        self.assertEqual(next(coords), (-1, 1))
        self.assertEqual(next(coords), (-1, 0))
        self.assertEqual(next(coords), (-1, -1))
        self.assertEqual(next(coords), (0, -1))
        self.assertEqual(next(coords), (1, -1))
        self.assertEqual(next(coords), (2, -1))

    def test_spiral_sum(self):
        self.assertEqual(spiral_sum("9"), 10)
        self.assertEqual(spiral_sum("25"), 26)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day03.txt")
    print("Part 1: ", manhattan_distance(txt))
    print("Part 2: ", spiral_sum(txt))
